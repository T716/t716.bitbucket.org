function SwitchPart(aPartShow, aPartHide) {
	$(aPartShow).show();
	$(aPartHide).hide();
}

function setContent(aContent) {
	var targetDiv = $('#targetCodeDiv');

	$(targetDiv).html(aContent);
}


function runTryItCode() {
	var lContent = $('#sourceCode').val();

	setContent(lContent);
};

function getSelectedCode() {
	var lData = '';
	
	$("div[id^='row']").each( function(index) {
		var lCheck = $(this).find("input:checked");
		
		if (lCheck.length == 1) {
			lData = lData + $(this).find("input[type='hidden']").val();
		}
	});

	// convert the &gt; and &lt; back to < and >
	lData = lData.replace(/(&lt;)/g, "<")		
	lData = lData.replace(/(&gt;)/g, ">");