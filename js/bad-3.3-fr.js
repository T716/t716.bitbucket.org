$(document).ready(function () {
	$("button#badsubmit").click(function() {
		if ($("input#fsy").val() > 2000 || $("input#fsy").val() === "") {
			alert("Vous avez commis une erreur dans le formulaire. S'il-vous-plaît évaluez.");
		} else if ($("input#fsm").val() === "" || $("input#b_day").val() === "" || $("input#sin").val() === "") {
			//sin and other fields cannot be empty - but we won't tell you! 
			alert("Un champ obligatoire est vide.");
		} else {
			alert("Félicitations");
		}
	})
});